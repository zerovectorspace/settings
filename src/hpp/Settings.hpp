#ifndef __SETTINGS__
#define __SETTINGS__

#include <string>
#include <unordered_map>
#include <vector>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <memory>
#include <mutex>

class Settings
{
    using String = std::string;
    using Sstream = std::stringstream;
    using Vec_c = std::vector<char>;
    using VVV_c = std::vector<std::vector<Vec_c>>;
    using u_ptr_settings = std::unique_ptr<Settings>;
    using Vec_str = std::vector<String>;

private:
    static u_ptr_settings S;
    static std::mutex build_mutex;
    std::unordered_map<String, String> user_map;
    std::unordered_map<String, std::vector<String>> default_map;

    String user_filename = "conf/Settings.conf";
    String default_filename = "conf/Default_Settings.conf";

    Vec_str numeric_opts = 
    {
        "option_2",
        "option_5"
    };

    Vec_str wildcard_opts = 
    {
        "option_4"
    };

public:
    static void build();

    static const String& get(const String& key);

    static const String& get_default(const String& key);

private:

    bool read_from_file(const char* db, Sstream& ss);

    Settings& construct_values(VVV_c& vvvc, const Sstream& ss);

    Settings& parse_values(const VVV_c& vvvc, bool is_default);

    Settings& test_option_validity();

    Settings& set_init_constants();

    template<typename OUT_TYPE, typename IN_TYPE>
    void split(std::vector<std::vector<OUT_TYPE>>& output_vec, IN_TYPE to_split, char delim, bool add_null = true)
    {
        Sstream iss((char*) to_split);
        String temp_str;
        std::vector<OUT_TYPE> temp_vec;

        output_vec.reserve(iss.str().size());
        while (std::getline(iss, temp_str, delim))
        {
            str_to_vec(temp_str, temp_vec, add_null);
            output_vec.push_back(temp_vec);
        }
    }

    template<typename IN_TYPE>
    std::vector<Vec_c> split(IN_TYPE to_split, char delim)
    {
        std::vector<Vec_c> output_vec;
        split(output_vec, to_split, delim);

        return output_vec;
    }

    template<typename T>
    void str_to_vec(String& str, std::vector<T>& vec, bool add_null = true)
    {
        vec.resize(str.size() + 1);
        vec = std::move(
            std::vector<T>{
                str.begin(),
                str.end()
            }
        );

        if (add_null)
            vec.push_back('\0');
    }

    bool is_inconsistent(const String& key, const String& val);

    bool is_wildcard_opt(const String& key);

    bool is_numeric_opt(const String& key);

    bool is_numeric_val(const String& val);

};

#endif
