#include "../hpp/Settings.hpp"

using String = std::string;

std::unique_ptr<Settings>
    Settings::S = std::make_unique<Settings>();

std::mutex Settings::build_mutex;


bool Settings::read_from_file(const char* db, Sstream& ss)
{
    int success = true;
    std::fstream file;
    file.open(db, std::ios::in);

    if (!file.good())
        return false;

    // read file line by line
    String line;
    while (std::getline(file, line, '\n'))
    {
        // if line is empty
        if (line.empty())
            continue;

        // remove spaces
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

        // if comment
        if (line.at(0) == '#' || (line.at(0) == '/' && line.at(1) == '/'))
            continue;

        // if it doesn't contain an '=' or '#'
        if (line.find('=') == std::string::npos || line.find('#') == std::string::npos)
            ss << line << ',';
        else
            std::cout << "Settings error: Line \"" << line << "\" not valid\n";

    }
    String str = ss.str();

    // std::transform(str.begin(), str.end(), str.begin(), ::tolower);

    ss = std::move(Sstream{str});

    if (ss.str().empty())
        success = false;

file.close();

return success;
}

Settings& Settings::construct_values(std::vector<std::vector<Vec_c>>& vvvc, const Sstream& ss)
{
    // split CSV to obtain vec<vec<char>>
    std::vector<Vec_c> components;
    split(components, ss.str().c_str(), ',');

    // for each
    for (const auto& i : components)
    {
        std::vector<Vec_c> c_no_eq;

        // split by '='
        split(c_no_eq, i.data(), '=');

        vvvc.push_back(c_no_eq);
    }

    return *this;
}

Settings& Settings::parse_values(const std::vector<std::vector<Vec_c>>& vvvc, bool is_default)
{
    for (const auto& pair : vvvc)
    {
        String key = pair[0].data();
        String val = pair[1].data();

        if (is_default)
            S->default_map[key].push_back(val);
        else
            S->user_map[key] = val;
    }

    return *this;
}

Settings& Settings::test_option_validity()
{
    for (auto& opt : S->user_map)
    {
        if (S->is_wildcard_opt(opt.first))
        {
            continue;
        }

        // If setting is numeric
        else if (S->is_numeric_opt(opt.first))
        {
            if (!S->is_numeric_val(opt.second))
            {
                String new_val = S->default_map[opt.first].at(0);

                std::cout << "\n[Settings Error] \"" <<
                    opt.first << " = " << opt.second <<
                    "\" is not numeric.\n\t Using default of \"" <<
                    new_val << "\"\n";

                opt.second = new_val;
            }
        }

        // If setting value is not one of the required values
        else if (S->is_inconsistent(opt.first, opt.second))
        {
            String new_val = S->default_map[opt.first].at(0);

            std::cout << "\n[Settings Error] \"" <<
                opt.first << " = " << opt.second <<
                "\" is not valid.\n\t Using default of \"" <<
                new_val << "\"\n";

            opt.second = new_val;
        }
    }

    return *this;
}

Settings& Settings::set_init_constants()
{
    return *this;
}

void Settings::build()
{
    std::lock_guard<std::mutex> lock(S->build_mutex);

    ////////////////////////////////
    // Construct Default Settings //
    ////////////////////////////////

    Sstream d_ss;
    if (!S->read_from_file(S->default_filename.c_str(), d_ss))
    {
        std::cout << "Settings: " << S->default_filename << " is empty\nExiting...\n";
        exit(1);
    }

    std::vector<std::vector<Vec_c>> d_vvvc;
    S->construct_values(d_vvvc, d_ss);

    S->parse_values(d_vvvc, true);

    /////////////////////////////
    // Construct User Settings //
    /////////////////////////////

    Sstream u_ss;
    if (!S->read_from_file(S->user_filename.c_str(), u_ss))
    {
        std::cout << "Settings: " << S->user_filename << " is empty. Using Defaults\n";
        for (const auto& opt : S->default_map)
            S->user_map[opt.first] = opt.second.at(0);
    }
    else
    {
        std::vector<std::vector<Vec_c>> u_vvvc;
        S->construct_values(u_vvvc, u_ss);

        S->parse_values(u_vvvc, false);

        S->test_option_validity();
        S->set_init_constants();
    }

    // for (const auto& i : S->user_map)
    // std::cout << i.first << " " << i.second << "\n";
}

const String& Settings::get(const String& key)
{
    if (S->user_map.count(key) == 0)
        std::cout << "[Settings Error] \"" << key << "\" does not exist.\n\n";

    return S->user_map.at(key);
}

const String& Settings::get_default(const String& key)
{
    if (S->default_map.count(key) == 0)
        std::cout << "[Settings Error] \"" << key << "\" does not exist.\n\n";

    return S->default_map.at(key).at(0);
}

bool Settings::is_inconsistent(const String& key, const String& val)
{
    auto& default_opt = S->default_map.at(key);

    return (std::find(
        default_opt.begin(),
        default_opt.end(),
        val) == default_opt.end());
}

bool Settings::is_wildcard_opt(const String& key)
{
    return std::find(
        wildcard_opts.begin(),
        wildcard_opts.end(),
        key) !=
        wildcard_opts.end();
}

bool Settings::is_numeric_opt(const String& key)
{
    return std::find(
        numeric_opts.begin(),
        numeric_opts.end(),
        key) !=
        numeric_opts.end();
}

bool Settings::is_numeric_val(const String& val)
{
    return val.find_first_not_of("-0123456789") == std::string::npos;
}
