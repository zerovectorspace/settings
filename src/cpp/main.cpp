#include "../hpp/Settings.hpp"

#include <iostream>
#include <string>

void static_usage()
{
    std::cout << "\n*** Testing Static Usage ***\n";

    std::cout << "\n";

    std::cout << "option_1 default = " << Settings::get_default("option_1") << "\n";
    std::cout << "option_1 = " << Settings::get("option_1") << "\n\n";

    std::cout << "option_2 default = " << Settings::get_default("option_2") << "\n";
    std::cout << "option_2 = " << Settings::get("option_2") << "\n\n";
    
    std::cout << "option_3 default = " << Settings::get_default("option_3") << "\n";
    std::cout << "option_3 = " << Settings::get("option_3") << "\n\n";

    std::cout << "option_4 default = " << Settings::get_default("option_4") << "\n";
    std::cout << "option_4 = " << Settings::get("option_4") << "\n\n";

    std::cout << "option_5 default = " << Settings::get_default("option_5") << "\n";
    std::cout << "option_5 = " << Settings::get("option_5") << "\n\n";

}

void instance_usage()
{
    std::cout << "\n*** Testing Instance Usage ***\n";

    // Settings can also be used as an instance
    Settings opts;

    std::cout << "\n";

    std::cout << "option_1 default = " << opts.get_default("option_1") << "\n";
    std::cout << "option_1 = " << opts.get("option_1") << "\n\n";

    std::cout << "option_2 default = " << opts.get_default("option_2") << "\n";
    std::cout << "option_2 = " << opts.get("option_2") << "\n\n";
    
    std::cout << "option_3 default = " << opts.get_default("option_3") << "\n";
    std::cout << "option_3 = " << opts.get("option_3") << "\n\n";

    std::cout << "option_4 default = " << opts.get_default("option_4") << "\n";
    std::cout << "option_4 = " << opts.get("option_4") << "\n\n";

    std::cout << "option_5 default = " << opts.get_default("option_5") << "\n";
    std::cout << "option_5 = " << opts.get("option_5") << "\n\n";
}

int main()
{
    /*
     * The map must be created manually
     * This allows us to get updated settings
     *   during runtime
     */
    Settings::build();

    // or
    // Settings opts;
    // opts.build();

    static_usage();
    instance_usage();
}
