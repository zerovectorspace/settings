# Settings

Define settings in plaintext files and use those settings at runtime in your application.

## Prerequisites

* CMake
* C++14

## Installation

* `git clone` this repository.
* `cmake /path/to/repository`
* `cmake && ./Settings`

## Usage

The class `Settings` is a singleton that can be used as an instance or statically. You can use one style or both simultaneously. `Settings::build()` uses a `std::lock_guard`.

```C++
int main()
{
    Settings opts;

    /*
        Settings::build() must be called once
        before using the get methods. This can
        be called on an instance or statically.
    */
    opts.build();

    std::cout << opts.get("option_1") << "\n";
    std::cout << Settings::get("option_1") << "\n";

    std::cout << opts.get_default("option_2") << "\n";
    std::cout << Settings::get_default("option_2") << "\n";
}
```

Define your settings in `conf/Default_Settings.conf` and override them in `conf/Settings.conf`.

```
# conf/Default_Settings.conf
option_1 = default_value
option_1 = second_available_value

// numeric
option_2 = 100

// wildcard
option_3 = This can be anything
```
```
# conf/Settings.conf
option_1 = second_available_value

option_2 = -100

option_3 = WILDCARD
```
